#version 430

layout (location = 0) in vec3 VertexPosition;
layout (location = 1) in vec3 VertexNormal;

layout (location = 0) out vec3 LightIntensity;

layout (location = 0) uniform vec4 LightPosition; // Light position in eye coords.
layout (location = 1) uniform vec3 Kd;            // Diffuse reflectivity
layout (location = 2) uniform vec3 Ld;            // Diffuse light intensity

layout (location = 3) uniform mat4 ModelViewMatrix;
layout (location = 4) uniform mat3 NormalMatrix;
layout (location = 5) uniform mat4 ProjectionMatrix;
layout (location = 6) uniform mat4 MVP;

void main()
{
    vec3 tnorm = normalize( NormalMatrix * VertexNormal);
    vec4 eyeCoords = ModelViewMatrix * vec4(VertexPosition,1.0);
    vec3 s = normalize(vec3(LightPosition - eyeCoords));

    LightIntensity = Ld * Kd * max( dot( s, tnorm ), 0.0 );

    gl_Position = MVP * vec4(VertexPosition,1.0);
}
